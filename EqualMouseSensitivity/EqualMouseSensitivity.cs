﻿using BepInEx;
using RoR2;
using UnityEngine;

namespace Memori
{
	[BepInDependency( "com.bepis.r2api" )]
	[BepInPlugin( "com.Memori.EqualMouseSensitivity", "Have your sprint mouse sensitivity behave as normal.", "1.0.3" )]
	public class EqualMouseSensitivity : BaseUnityPlugin
	{
		public void Awake() {
			On.RoR2.CameraRigController.Update += UpdateCameraRigController;
		}

		/// <summary>
		/// Tricks the CameraRigController update function into thinking the character isn't sprinting, causing it to never apply the camera slow.
		/// </summary>
		/// <param name="orig"></param>
		/// <param name="self"></param>
		public void UpdateCameraRigController( On.RoR2.CameraRigController.orig_Update orig, CameraRigController self ) {
			if( Time.deltaTime == 0f || !self.target )
				return;

			CharacterBody targetBody = self.target.GetComponent<CharacterBody>();

			if( !targetBody )
				return;

			bool isSprinting = targetBody.isSprinting;

			if( isSprinting ) {
				targetBody.isSprinting = false;
				orig( self );
				targetBody.isSprinting = true;

				// This is the second part of code that gets skipped if isSprinting=false.
				// Though it works fine without this code, behaviour is best kept as original as possible.
				if( self.sprintingParticleSystem ) {
					ParticleSystem.MainModule main = self.sprintingParticleSystem.main;

					if( isSprinting ) {
						main.loop = true;

						if( !self.sprintingParticleSystem.isPlaying )
							self.sprintingParticleSystem.Play();
					}
					else {
						main.loop = false;
					}
				}
			}
			else {
				orig( self );
			}
		}
	}
}